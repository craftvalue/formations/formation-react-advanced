import React from "react";

/**
 * TODO-EXO(exo-story)
 * Dans cet exercice vous allez rajouter une story pour le
 * composant PokemonCard.
 *
 * 1. Ajouter le export default avec les bonnes propriétés
 * 2. Créer un template pour ce composant.
 * 3. Créer une story pour ce composant.
 */

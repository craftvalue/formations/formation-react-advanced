---
sidebar_position: 1
objective: Rappeler les bases de l'écosystème JavaScript et Browser
---

# Introduction

## Vocabulaire

**JavaScript:** langage de développement non typé. Il est exécuté par le navigateur
afin d'ajouter des comportements sur un page web.

**TypeScript:** langage de développement typé développé par Microsoft. Il est
transpilé vers JavaScript avant exécution.

**ECMAScript:** spécifications du langage JavaScript. Chaque spécification
apporte un nouveau lot de fonctionnalités au langage JavaScript.
On parle d'ES6, ES7, ES2020 etc. Une version d'un navigateur ou de NodeJS
supporte une version d'ECMAScript : cela veut dire qu'un navigateur ne supporte
pas nécessairement toutes les nouvelles fonctionnalités.

**[V8][v8]:** moteur d'exécution de JavaScript développé par Google. Il est utilisé
par Google Chrome (entre autre) et permet une exécution très performante du
code JavaScript.

**NodeJS:** plateforme d'exécution de code JavaScript dans un environnement
natif (sans navigateur) et directement sur une machine (serveur, ordinateur
portable). Utilise en interne V8 et libuv, et introduit le concept d'[event
loop][eventloop]. Propose en un ensemble de librairies natives (crypto, http,
https etc...). <br /> Équivalent : [Deno](https://deno.land/)

**Package (paquet):** bibliothèque de code JavaScript facilitant la
réutilisation dans plusieurs projets.

**Gestionnaire de paquet:** écosystème offrant une interface simple
d'utilisation permettant de publier des paquets, de récupérer et utiliser des
paquets existants. Composé d'une CLI (commande `npm` ou `yarn` par exemple) et
service stockage des paquets (ex: [npmjs.com][npmjs] ou
[yarnpkg.com][yarnpkg]).

**[React][react]:** librairie développée par Facebook afin de faciliter le
développement de Single Page Application et plus généralement
d'applications web.

CLI utiles:

- [nvm](https://github.com/nvm-sh/nvm): permet de gérer plusieurs versions de
  NodeJS différentes
- [npx](https://www.npmjs.com/package/npx): exécute une commande NodeJS
  depuis `node_modules/.bin` ou le cache central, et installe au besoin les
  paquets nécessaires si non présents.

## Développer pour le web

En local, on dispose d'un environnement NodeJS qui permet d'exécuter du code
JavaScript. On utilise un gestionnaire de paquet (npm ou yarn) afin de
récupérer des paquets.

Lorsqu'on développe pour le web et pour les navigateurs, il est souvent
nécessaire de _transpiler_ et de _packager/bundler_ le code JavaScript
développé pour les navigateurs pour les raisons suivantes :

- JavaScript est évolutif et on veut que les anciennes versions de navigateurs
  puisse également exécuter notre application : les fonctionnalités
  ES6 ou ES7 sont réécrites au format ES5 (support plus important) ;
- optimisation : il est moins couteux pour un navigateur de récupérer quelques
  fichiers que des milliers ([domain sharding][]) ;
- minification : on peut réduire la taille du code (suppression des
  commentaires, des espaces, obfuscation des noms de variables).

Pour cela on utilise des bibliothèques comme `webpack` ou `rollup`. Par exemple
React utilise webpack via `create-react-app`. Ces librairies via des extensions
permettent également de gérer les autres ressources web (polices/fonts,
css/scss/less, images etc).

`create-react-app` permet de mettre en place une application ReactJS avec
ensemble de bibliothèques déjà configurées afin de réduire le coût de mise en
place initial. D'autres alternatives existent comme
[ViteJS](https://vitejs.dev/) qui est, lui, basé sur rollup.

## Asynchronicité

Les environnements d'exécution de JavaScript (navigateur et NodeJS) ne
disposent que d'un thread principal par design[^1]. Afin de ne pas bloquer les
interactions sur la page, on utilise la notion d'asynchronicité et les
fonctionnalités misent à disposition à cet effet pour gérer les parties
couteuses : gestion de l'I/O (fichiers, requêtes), calcul longs.

[v8]: https://v8.dev/
[eventloop]: https://nodejs.org/en/docs/guides/event-loop-timers-and-nexttick/
[nodejs api]: https://nodejs.org/api/
[npmjs]: https://www.npmjs.com/
[yarnpkg]: https://yarnpkg.com/
[react]: https://reactjs.org/ "ReactJS"
[domain sharding]: https://developer.mozilla.org/en-US/docs/Glossary/Domain_sharding "Domain
Sharding"

[^1]:
    ce n'est pas vrai dans l'absolu, il y a un thread principal et des threads
    supplémentaires avec NodeJS mais on admet et garde la simplification ici.

---
sidebar_position: 100
---

# Factorisation des comportements

## High Order Components (HOCs)

Un composant d'ordre supérieur (High Order Component ou HOC) est fonction
retournant soit un composant, soit un HOC.

:::danger
Ne jamais instancier un HOC à l'intérieur d'un `render` ou lors du rendu d'un
composant.

React se base sur le type (référence de la fonction) du composant lors de la
réconciliation des hooks. Si les types sont différents, les valeurs des hooks
sont supprimés.

Dans le code ci-dessous, `Comp` à une **valeur** différente à chaque rendu du
composant.

```typescript
import DetailView from "./DetailView";

function configure(Component) {
  return (props) => {
    return <Component config={myConfig} {...props} />;
  };
}

export function MyComponent() {
  const Comp = configure(DetailView);

  return <Comp myProp={10} />;
}
```

:::

## Custom hooks

## Comparaison

| Fonctionnalité            | HOC                                            | Hooks      |
| ------------------------- | ---------------------------------------------- | ---------- |
| Carte mentale et création | Complexe (fonctions d'ordre 1 et 2)            | Simple     |
| Dépendances               | Implicites                                     | Explicites |
| Typage                    | Complexe                                       | Simple     |
| Typage à l'utilisation    | Nécessaire d'importer les types pour les props | Implicite  |

---
sidebar_position: 1
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# Composants réutilisables

Une des problèmatiques assez courante sur les gros projets est de créer un
ensemble de composants facilement réutilisables. Cette partie a vocation à
proposer tout un ensemble de technique qui permettent de faciliter
l'adaptibilité des composants à tout un tas de situation.

## HTML Props

Par défaut, proposez des composants facilement configurables via les props HTML
classiques :

<Tabs>
<TabItem value="js" label="JavaScript">

```jsx
import classnames from "classnames";

function LoginForm(props) {
  const { onSubmit, className, ...otherProps } = props;

  return (
    <form {...otherProps} className={classnames("form", className)}>
      <Input name="email" type="text" label="Email" />
      <Input name="password" type="password" label="Password" />
    </form>
  );
}
```

</TabItem>
<TabItem value="ts" label="TypeScript">

```tsx
import classnames from "classnames";

function LoginForm(props: React.ComponentProps<"form">): JSX.Element {
  const { onSubmit, className, ...otherProps } = props;

  return (
    <form {...otherProps} className={classnames("form", className)}>
      <Input name="email" type="text" label="Email" />
      <Input name="password" type="password" label="Password" />
    </form>
  );
}
```

</TabItem>
</Tabs>

## Conflits de classes CSS

Utilisez les CSS modules ou des librairies de CSS-in-js.

## Utilisez

<!--
Utiliser toutes les props
CSS Modules class names clashes
React.forwardRef
children prop
children prop as function
renderProp
as prop
-->

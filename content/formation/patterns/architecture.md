---
sidebar_position: 3
---

# Architecture

## Arborescence

Il est vivement conseillé d'utiliser une arborescence par fonctionnalité :

```
src/
|--/components/                                       # Composants partagés
|--/features/
|-----------/auth/{components,api,contexts,helpers}   # Authentification
|-----------/products/{components,api,helpers...}     # Produits
|-----------/order/{components,api,helpers}           # Commande et panier
|--/pages/ 
```

Pourquoi :

- il est facile de se repérer et de localiser une fonctionnalité ;
- ça permet d'éviter le couplage entre deux fonctionnalités ou de le repérer
  rapidement ;
- on peut facilement évaluer le coût d'une fonctionnalité donnée.

## Design System

Avec l'arborescence ci-dessus, l

<!--
Design system
storybook
-->

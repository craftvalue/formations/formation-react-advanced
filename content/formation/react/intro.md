---
sidebar_position: 1
---

# React

Librairie de développement d'interface web et de Single Page Application[^1]
développée par Facebook.

## Concepts

### Composants

React permet de définir des Composants :

- un composant est une fonction prenant des propiétés en entrée `props` ;
- et retournant du JSX en fonction d'un éventuel état (`state`) interne.

Par abus de langage on appelle indifféremment Composant la fonction et l'usage
effectif du composant : `<Composant />`.

### Création d'un composant

```js
function Composant(props) {
  return <div>{props.name}</div>;
}
```

### JSX

Le JSX est du sucre syntaxique autour de la primitive React
`React.createElement`.

```js
<Detail prop1={value} />;
// est équivalent à
React.createElement(Detail, { prop1: value });
```

Cela rend React particulièrement puissant car il ne s'agit que de JavaScript :
nul besoin d'apprendre un langage de template par exemple.

### React.Fragment

Comme React n'accepte qu'un seul et unique élément en retour d'un composant, il
est utile d'avoir recours aux fragments :

```js
function HelloWorld() {
  return (
    <React.Fragment>
      <span>Hello</span>
      <span>World</span>
    </React.Fragment>
  );
}
```

Note: on peut abréger `<React.Fragment>...</React.Fragment>` par `<>...</>`.

## Fonctionnalités

### Context

Afin de partager l'état a des enfants éloignés au sein du JSX, on peut
utiliser le concept de Contexte.

```js
const AuthContext = React.createContext({});

export function AuthContextProvider({ children }) {
  const [auth, setAuth] = React.useState({});
  return <AuthContext.Provider value={auth}>{children}</AuthContext.Provider>;
}

export function useAuthContext() {
  const auth = React.useContext(AuthContext);
  if (!auth) throw new Error("Should be used with AuthContextProvider");
  return auth;
}

function App() {
  return (
    <AuthContextProvider>
      <Routes />
    </AuthContextProvider>
  );
}
```

### Hooks

Avec la version [16.8](https://reactjs.org/blog/2019/02/06/react-v16.8.0.html)
React introduit le concept de Hooks ce qui permet de simplifier la création de
composant stateful (utilisant un état interne) et d'éviter d'avoir recours à
des Class Components.

#### React.useState

```jsx live
function WithState() {
  const [state, setState] = React.useState(0);
  return (
    <>
      <button
        type="button"
        onClick={() => setState((currentState) => currentState + 1)}
        style={{ marginRight: 20 }}
      >
        +1
      </button>
      State: {state}
    </>
  );
}
```

#### React.useEffect

```jsx live
function WithEffect() {
  const getSize = () => {
    return { height: window.innerHeight, width: window.innerWidth };
  };
  const [size, setSize] = React.useState(getSize);
  const myResizeHandler = React.useCallback((e) => {
    setSize(getSize());
  }, []);
  React.useEffect(() => {
    window.addEventListener("resize", myResizeHandler);
    return () => window.removeEventListener("resize", myResizeHandler);
  }, [myResizeHandler]);

  return <div>{JSON.stringify(size)}</div>;
}
```

Choses importantes à noter :

- la fonction de `cleanup` (retour du premier argument) est appellée avant
  chaque `render` et si les dépendances sont différentes ;
- la liste de dépendances indique quand relancer l'effet (si l'une des
  dépendances a changée) ;
- la liste des dépendances fait une comparaison `shallow` (superficielle :
  comprendre elle ne compare pas les valeurs, sauf scalaires, mais les
  références).

#### React.useRef

```js
// useRef
function Comp() {
  const elementRef = React.useRef(null);
  elementRef.current?.getBoundingClientRect();

  return <div ref={elementRef} />;
}
```

#### React.useEffect

```jsx live noInline
const initialStateArg = { a: 2 };
function reducer(prevState, action) {
  switch (action.type) {
    case "a":
      return { ...prevState, a: prevState.a + 1 };
    case "b":
      return { ...prevState, a: prevState.a - 1, b: prevState.b + 1 };
    default:
      return prevState;
  }
}

function createInitialState(arg) {
  return {
    a: 0,
    b: 0,
    ...arg,
  };
}

function WithReducer() {
  const [state, dispatch] = React.useReducer(
    reducer,
    initialStateArg,
    createInitialState
  );
  return (
    <div>
      {JSON.stringify(state)}
      <br />
      <button type="button" onClick={() => dispatch({ type: "a" })}>
        Dispatch a
      </button>
      <button type="button" onClick={() => dispatch({ type: "b" })}>
        Dispatch b
      </button>
    </div>
  );
}

render(<WithReducer />);
```

#### Rules of Hooks

- Uniquement utilisable au sein des Functional Components ou de Custom Hooks ;
- Toujours utilisés dans le corps des fonctions (pas dans des boucles ou dans des
  conditions).

=> React utilise des effets de bords pour conserver l'état interne des hooks et
l'ordre dans lesquels ils ont été appelés pour chaque composant.

[^1]: mais permet aussi de créer des vidéos, des sites web statiques etc.

```

```

// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require("prism-react-renderer/themes/github");
const darkCodeTheme = require("prism-react-renderer/themes/dracula");

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: "Formation React Avancé",
  url: "https://craftvalue.gitlab.io",
  baseUrl: "/",
  onBrokenLinks: "throw",
  onBrokenMarkdownLinks: "warn",
  favicon: "img/favicon.ico",
  organizationName: "CraftValue", // Usually your GitHub org/user name.
  projectName: "formation-react-advanced", // Usually your repo name.
  i18n: {
    defaultLocale: "fr",
    locales: ["fr"],
  },

  themes: ["@docusaurus/theme-live-codeblock"],
  presets: [
    [
      "@docusaurus/preset-classic",
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve("./sidebars.js"),
          // Please change this to your repo.
          path: "formation",
          routeBasePath: "formation",
          editUrl:
            "https://gitlab.com/craftvalue/formations/formation-react-advanced/-/edit/main/content/",
        },
        theme: {
          customCss: require.resolve("./src/css/custom.css"),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: "React Avancé",
        logo: {
          alt: "Formation React Avancé",
          src: "img/logo.svg",
        },
        items: [
          {
            type: "doc",
            docId: "intro",
            position: "left",
            label: "Formation",
          },
          {
            href: "https://gitlab.com/craftvalue/formations/formation-react-advanced",
            label: "GitLab",
            position: "right",
          },
        ],
      },
      footer: {
        style: "dark",
        copyright: `CC BY-SA ${new Date().getFullYear()} CraftValue. Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
